/*
A*
AOJ 1281: The Morning after Halloween  
http://judge.u-aizu.ac.jp/onlinejudge/description.jsp?id=1281
*/

#include<iostream>
#include<sstream>
#include<algorithm>
#include<map>
#include<set>
#include<queue>
#include<complex>
#include<cstdio>
#include<cstdlib>
#include<cstring>
// #define DEBUG
    
#define Y first
#define X second
    
using namespace std;
    
// typedef complex<double>P;
    
typedef long long int ll;
typedef unsigned long long int ull;
typedef unsigned char uchar;
   
typedef pair<uchar,uchar>P;
    
const short INF = (1 << 14);
const double EPS=1e-9;
const short MAX = 16;
    
short W,H,N;
char field[MAX][MAX];
short heuristics[3][MAX][MAX];
short d[MAX][MAX][MAX][MAX][MAX][MAX];
P starts[3],goals[3];
    
const char dx[] = {0,1,0,-1,0};
const char dy[] = {1,0,-1,0,0};
    
void init(){
  
  for(int i = 0 ; i < 3 ; i++)
    for(int j = 0 ; j < MAX ; j++)
      for(int k = 0 ; k < MAX ; k++)
	heuristics[i][j][k] = INF;
   
  for(int i = 0 ; i < MAX ; i++)
    for(int j = 0 ; j < MAX ; j++)
      for(int k = 0 ; k < MAX ; k++)
	for(int ii = 0 ; ii < MAX ; ii++)
	  for(int jj = 0 ; jj < MAX ; jj++)
	    for(int kk = 0 ; kk < MAX ; kk++)
	      d[i][j][k][ii][jj][kk] = INF;
}
    
bool input(){
  string str;
  cin >> W >> H >> N;
  getline(cin,str);
  if(W == 0 && H == 0 && N == 0)return true;  
  for(int i = 0 ; i < H ; i++){
    getline(cin,str);
    for(int j = 0 ; j < W ; j++){
      field[i][j] = str[j];
      if(isalpha(field[i][j])){
	int id;
	if(isupper(field[i][j])){
	  id = (field[i][j] - 'A');
	  goals[id] = P(i,j);
	}
	else {
	  id = (field[i][j] - 'a');
	  starts[id] = P(i,j);
	}
      }
    }
  }
  return false;
}
    
namespace bfs{
  typedef pair<short,P> State;
  inline void bfs(uchar id){
    queue<State>que;
    que.push(State(0,goals[id]));
    heuristics[id][goals[id].Y][goals[id].X] = 0;
    while(que.size()){
      State state = que.front(); que.pop();
      for(int i = 0 ; i < 4 ; i++){
	int ny = state.second.Y + dy[i];
	int nx = state.second.X + dx[i];
	if(0 <= nx && nx < W && 0 <= ny && ny < H){
	  if(field[ny][nx] != '#'){
	    if(heuristics[id][ny][nx] > heuristics[id][state.second.Y][state.second.X] + 1){
	      heuristics[id][ny][nx] = heuristics[id][state.second.Y][state.second.X] + 1;
	      que.push(State(heuristics[id][ny][nx],P(ny,nx)));
	    }
	  }
	}
      }
    }
  }
}
    
namespace A_star{
  class State{
  public:
    P p[3];
    short cost;
    short h_cost;    
    State(){
      cost = h_cost = 0;
      for(int i = 0 ; i < 3 ; i++)p[i] = P(-1,-1);
    }
    State(short _cost,P *_p){
      cost = _cost;
      for(int i = 0 ; i < 3 ; i++)p[i] = P(-1,-1);
      for(int i = 0 ; i < N ; i++)p[i] = _p[i];
      calc_h_cost();
    }
    
    inline void calc_h_cost(){
      for(int i = 0 ; i < N ; i++){
	h_cost += heuristics[i][p[i].Y][p[i].X] ;
      }
      h_cost /= 3;
      h_cost += cost;
    }
    
    inline bool operator < (const State& s)const{
      if(h_cost == s.h_cost)return cost > s.cost;
      return h_cost > s.h_cost;
    }
    
    inline bool isCollision(){
      for(int i = 0 ; i < N ; i++){
	if(field[p[i].Y][p[i].X] == '#')return true;
      }
      for(int i = 0 ; i < N ; i++){
	for(int j = i+1 ; j < N ; j++){
	  if(p[i] == p[j])return true;
	}
      }
      return false;
    }
    
    inline bool isGoal(){
      for(int i = 0 ; i < N ; i++)if(goals[i] != p[i])return false;
      return true;
    }
       
    inline void print(){
      cout << "cost = " << (int)(cost) << " h_cost = " << (int)(h_cost) << endl;
      for(int i = 0 ; i < N ; i++){
	cout << "i = " << i << " (X,Y) : " << (int)(p[i].X) << ' ' << (int)(p[i].Y) << endl;
      }
    }
  };
     
  inline void move(State& s,uchar now,const uchar order[],priority_queue<State>&que,const int& pre_cost){
    if(now == N){      
      short &d_alias = d[s.p[0].Y][s.p[0].X][s.p[1].Y][s.p[1].X][s.p[2].Y][s.p[2].X];
      if(d_alias > s.h_cost){    
	d_alias = s.h_cost; 
	que.push(s);    
      }
      return;
    }    
    for(int i = 0 ; i < 5 ; i++){
      State ns = s;
      ns.p[order[now]].X += dx[i];
      ns.p[order[now]].Y += dy[i];
      if(ns.isCollision())continue;
      ns.calc_h_cost();
      if(d[ns.p[0].Y][ns.p[0].X][ns.p[1].Y][ns.p[1].X][ns.p[2].Y][ns.p[2].X] < ns.h_cost)continue;
      move(ns,now+1,order,que,pre_cost);
    }
  }
   
  short A_star(){
    short ret = INF;
    State state(0,starts);
    d[state.p[0].Y][state.p[0].X][state.p[1].Y][state.p[1].X][state.p[2].Y][state.p[2].X] = state.h_cost;
    priority_queue<State>que;
    que.push(state);
    uchar order[3] = {0,1,2};
    while(que.size()){
      State s = que.top(); que.pop();
      short &d_alias = d[s.p[0].Y][s.p[0].X][s.p[1].Y][s.p[1].X][s.p[2].Y][s.p[2].X];
      if(ret <= s.h_cost)continue;
      if(s.h_cost <= d_alias){
	if(!s.isGoal()){
	  s.cost++;
	  do{
	    move(s,0,order,que,s.cost);
	  }while(next_permutation(order, order + N));
	}
	else {
	  ret = min(ret,s.cost);
	}
      }
    }
    return ret;
  }
}
   
int main(int argc, char *argv[])
{
  while(1){
    init();
    if(input())break;
    for(int i = 0 ; i < N ; i++)bfs::bfs(i);
    cout << A_star::A_star() << endl;
  }
  return 0;
}
