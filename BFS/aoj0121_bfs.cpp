/*
http://judge.u-aizu.ac.jp/onlinejudge/description.jsp?id=0121
幅優先探索
*/

#include<iostream>
#include<algorithm>
#include<map>
#include<queue>
#include<string>
 
using namespace std;
 
int d[4] = {1,-1,4,-4};
 
int main(void){
   
  string str = "01234567";
  map<string,int>move;
  queue<string>Q;
 
  move[str] = 0;
  Q.push(str);
 
  while(!Q.empty()){
    string q = Q.front();
    Q.pop();
    int now = q.find("0");
    for(int i = 0 ; i < 4 ; i++){
 
      if(now % 4 == 3 && i == 0)continue;
      if(now % 4 == 0 && i == 1)continue;
      if(now >= 4 && i == 2)continue;
      if(now < 4 && i == 3)continue;
 
      string temp = q;
      swap(temp[now],temp[now + d[i]]);
      if(move.find(temp) != move.end())continue;
 
      move[temp] = move[q] + 1;
      Q.push(temp);
    }
  }
 
  while(getline(cin,str)){
    str.erase(remove(str.begin(),str.end(),' '),str.end());
    cout << move[str] << endl;
  }
  return 0;
}
