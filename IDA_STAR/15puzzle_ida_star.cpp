/*
　反復深化法
　１５パズル　

sample input

in.txt
------------------
8 2 10 15
9 13 3 4
14 1 11 0 
6 5 12 7

実行例
-------------
g++ 15puzzle.cpp
./a.out < in.txt
-------------
*/

#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<ctime> // for clock()
#include<cstdio>
using namespace std;

int data[4][4];

//方向ベクトル
const int dy[4] = {-1,0,1,0};
const int dx[4] = {0,1,0,-1};

vector<int>path;

// 描画処理
void display(int a[4][4]){
  for(int i = 0 ; i < 4 ; i++){
    for(int j = 0 ; j < 4 ; j++){
      cout << a[i][j] << ' ' ;
    }
    cout << endl;
  }
  cout << endl;
}

//与えられたパズルの解法が存在するかどうかを判定
bool canSolve(int x,int y){
  int tmp[4][4];
  // copy
  for(int i = 0 ; i < 4 ; i++){
    for(int j = 0 ; j < 4 ; j++){
      tmp[i][j] = data[i][j];
    }
  }
  int cnt = 0;

  // 0を所定の場所へ移動させる
  while(x < 3)swap(tmp[y][x],tmp[y][x+1]),x++;
  while(y < 3)swap(tmp[y][x],tmp[y+1][x]),y++;

  for(int i = 0 ; i < 16 ; i++){
    int x,y,num;
    x = i%4;
    y = i/4;
    num = i+1;
    if(num == 16)num = 0;
    if(tmp[y][x] == num)continue;

    bool f = true;
    for(int j = 0 ; j < 4 ; j++){
      for(int k = 0 ; k < 4 ; k++){
	if(f){ // init
	  j = y;
	  k = x+1;
	  f = false;
	}
	if(num == tmp[j][k]){
	  swap(tmp[y][x],tmp[j][k]);
	  cnt++;
	  j = 4;
	  break;
	}
      }
    }
  }
  return (cnt+1)%2;
}

// 入力処理
void input(int &x,int &y){

  cout << "Please input 15 puzzle data" << endl;
  cout << "Blank is 0" << endl;

  for(int i = 0 ; i < 4 ; i++){
    for(int j = 0 ; j < 4 ; j++){
      cin >> data[i][j];
      if(data[i][j] == 0){
	y = i;
	x = j;
      }
    }
  }

  cout << "Input is end." << endl;

}

// パズルが完成したかどうかを判定
bool check(){
  for(int i = 0 ; i < 15 ; i++){
    if(data[i/4][i%4] != i+1)return false;
  }
  return true;
}

// ヒューリスティック関数
int heuristic(){
  int h = 0;
  for(int i = 0 ; i < 16 ; i++){
    int x1,y1,x2,y2,num;
    x1 = i%4;
    y1 = i/4;
    num = data[y1][x1];
    if(num == 0)num = 16;
    num--;
    x2 = num%4;
    y2 = num/4;
    h += abs(x1 - x2) + abs(y1 - y2);
  }
  return h;
}

//　反復深化を行う関数
bool dfs(int cnt,int x,int y,int lim,int pre){

  if(cnt + heuristic() >= lim)return false;
  if(check()){
    return true;
  }
  for(int i = 0 ; i < 4 ; i++){
    int nx,ny,tmp;
    nx = x + dx[i];
    ny = y + dy[i];
    if(!((0 <= nx && nx < 4) && (0 <= ny && ny < 4)))continue;
    if(data[ny][nx] == pre)continue;
      
    tmp = data[ny][nx];

    swap(data[y][x],data[ny][nx]);
    if(dfs(cnt+1,nx,ny,lim,tmp)){
      swap(data[y][x],data[ny][nx]);
      path.push_back(i);
      return true;
    }
    swap(data[y][x],data[ny][nx]);
  }
  return false;
}

// 初期化
void init(){
  path.clear();
}

// 探索で見つけたパスにしたがってシミュレーションしながら解法を表示する
void displayPath(int now,int x,int y){
  cout << "now = " << now << endl;
  display(data);

  if(now == path.size()){
    cout << "Display is end" << endl;
    return;
  }

  int nx,ny;
  nx = x + dx[path[now]];
  ny = y + dy[path[now]];
  swap(data[y][x],data[ny][nx]);
  displayPath(now+1,nx,ny);
}

void solve(){

  int x,y;
  input(x,y);
  cout << "Check whether this puzzle data is solvable or not?" << endl;

  if(!canSolve(x,y)){
    cout << "This puzzle is not solvable." << endl;
    return;
  }
  cout << "This puzzle is solvable" << endl;
  cout << "Search the solution" << endl;

  clock_t oldTime, newTime;
  oldTime = clock();

  for(int lim = heuristic() ; ; lim++){
    if(dfs(0,x,y,lim,-1))break;
  }
   
  newTime = clock();

  cout << "Search is end" << endl;
  printf("time = %.3f \n",(float)(newTime - oldTime) / (float)CLOCKS_PER_SEC);

  cout << "Display the path" << endl;

  reverse(path.begin(),path.end());

  displayPath(0,x,y);
  cout << "Puzzle is solved!!" << endl;
}

int main(){
  init();
  solve();
  return 0;
}
