/*
  双方向BFS
 AOJ 1281: The Morning after Halloween  
 http://judge.u-aizu.ac.jp/onlinejudge/description.jsp?id=1281
*/

#include<iostream>
#include<string>
#include<vector>
#include<map>
#include<queue>
#include<cstring>
using namespace std;

vector<string>field;

const int INF = 1e8;
int W,H,N;
const int SIZE = 16;
typedef unsigned short uchar;
typedef pair<int,int>P;
typedef pair<uchar,vector<P> >PP;
const uchar NOT_VISITED = (~0);
vector<P>points[2];
uchar d[SIZE][SIZE][SIZE][SIZE][SIZE][SIZE];
char vis[SIZE][SIZE][SIZE][SIZE][SIZE][SIZE];
int push_count;


const int dy[] = {1,0,-1,0,0};
const int dx[] = {0,1,0,-1,0};

void input(){
  cin.ignore();
  field = vector<string>(H);
  for(int i = 0 ; i < H ; i++)getline(cin,field[i]);
  for(int i = 0 ; i < 2 ; i++)points[i] = vector<P>(3,P(0,0));
  for(int i = 0 ; i < H ; i++){
    for(int j = 0 ; j < W ; j++){
      if(isalpha(field[i][j])){
	if(isupper(field[i][j]))points[1][field[i][j]-'A'] = P(i,j);
	else points[0][field[i][j]-'a'] = P(i,j);
      }
    }
  }
}

inline int calcNext(int now,int cnt,PP &pp,queue<PP>&que,bool used[3],int id){
  used[now] = true;
  int ret = -1;
  P tp = pp.second[now];
  for(int i = 0 ; i < 5 ; i++){
    P& np = pp.second[now];
    np = P(np.first + dy[i],np.second + dx[i]);
    if(field[np.first][np.second] == '#')goto NEXT;
    for(int j = 0 ; j < N ; j++){
      if(j == now)continue;
      if(pp.second[j] == np)goto NEXT;
    }
    if(cnt+1 == N){
      vector<P>& v = pp.second;
      uchar &dist = d[v[0].first][v[0].second][v[1].first][v[1].second][v[2].first][v[2].second];
      char &visited = vis[v[0].first][v[0].second][v[1].first][v[1].second][v[2].first][v[2].second];

      if(!visited && id == 1){
	visited = true;
	que.push(pp);   
      }

      if(dist == NOT_VISITED){	    
	if(id == 0){
	  dist = pp.first;
	  push_count++;
	  que.push(pp);	    
	}
      }
      else {
	if(id == 1){
	  return pp.first + dist;
	}
      }
    }
    else {
      for(int j = 0 ; j < N ; j++){
	if(used[j])continue;
	ret = calcNext(j,cnt+1,pp,que,used,id);
	if(ret != -1)return ret;
      }
    }
  NEXT:;
    pp.second[now] = tp;
  }
  used[now] = false;
  return ret;
}

int bfs(int id){
  int ret = -1;

  queue<PP>que;
  que.push(PP(0,points[id]));

  char & visited = vis[points[id][0].first][points[id][0].second][points[id][1].first][points[id][1].second][points[id][2].first][points[id][2].second];

  uchar & dist = d[points[id][0].first][points[id][0].second][points[id][1].first][points[id][1].second][points[id][2].first][points[id][2].second];

  if(id == 1){
    if(dist != NOT_VISITED){
      return dist;
    }
  }

  if(id == 0)dist = 0;
  visited = true;

  while(que.size()){
    PP pp = que.front(); que.pop();
    if(id == 0 && pp.first > 70)break;
    bool used[3];
    for(int i = 0 ; i < N ; i++)used[i] = false;
    pp.first++;
    for(int i = 0 ; i < N ; i++){
      if(used[i])continue;	 
      ret = calcNext(i,0,pp,que,used,id);
      if(ret != -1)return ret;
    }
  }
  return ret;
}

int main(){
  while(cin >> W >> H >> N,W|H|N){
    memset(d,NOT_VISITED,sizeof(d));
    memset(vis,0,sizeof(vis));
    input();
    int res;
    push_count = 0;
    for(int i = 0 ; i < 2 ; i++){
      res = bfs(i);
    }
    cout << res << endl;
  }
  return 0;
}
