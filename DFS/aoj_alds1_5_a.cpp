/*
http://judge.u-aizu.ac.jp/onlinejudge/description.jsp?id=ALDS1_5_A
dfs
*/

#include<iostream>
using namespace std;
 
const int MAX = 2001;
int data[MAX];
 
int N,M;
 
bool dfs(int sum,int pos){
  if(sum == M)return true;
  if(pos == N)return false;
  if(dfs(sum,pos+1))return true;
  if(dfs(sum + data[pos],pos+1))return true;
  return false;
}
 
int solve(){
  for(int i = 0 ; i < N ; i++)cin >> data[i];
  int m;
  cin >> m;
  for(int i = 0 ; i < m ; i++){
    cin >> M;
    if(dfs(0,0))cout << "yes" << endl;
    else cout << "no" << endl;
  }
 
}
 
int main(){
  while(cin >> N){
    solve();
  }
  return 0;
}
